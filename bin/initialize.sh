#!/bin/bash

source "$(realpath "${BASH_SOURCE%/*}/libs/env")"
source "${ORIGINAL_SCRIPT_PATH}/libs/debug"
source "${ORIGINAL_SCRIPT_PATH}/libs/functions"
source "${ORIGINAL_SCRIPT_PATH}/libs/python_functions"

cd "${ORIGINAL_SCRIPT_PATH}" || exit 1
cd .. || exit 1

print_execution_environment_information

print_python_execution_environment

echo

# Initialize Python virtual environment
#######################################

python3 -m venv venv --clear

source venv/bin/activate

curl https://bootstrap.pypa.io/get-pip.py | python

pip install --upgrade pip
pip install --upgrade setuptools

pip install -r requirements.txt || echo "No requirements were installed"
