#!/bin/bash

source "$(realpath "${BASH_SOURCE%/*}/libs/env")"
source "${ORIGINAL_SCRIPT_PATH}/libs/debug"
source "${ORIGINAL_SCRIPT_PATH}/libs/functions"
source "${ORIGINAL_SCRIPT_PATH}/libs/python_functions"

cd "${ORIGINAL_SCRIPT_PATH}" || exit 1
cd .. || exit 1

print_random_banner

# Initialize python venv if missing
###################################

if [[ ! -d "venv" ]]; then
    echo && echo "${BOLD}venv folder not found${RESET}. Initializing the environment..."
    "${ORIGINAL_SCRIPT_PATH}/initialize.sh" && wait
    echo && echo "${BOLD}venv created${RESET}. Continuing execution of ${ORIGINAL_SCRIPT_NAME}..."
fi

# Setup environment
###################

source venv/bin/activate
export ENVIRONMENT_NAME="dev"
export PYTHONPATH="." ## Load all Python packages from root and upwards.

print_execution_environment_information
print_python_execution_environment

echo

## Run the program
##################

echo "[${BOLD}RUNNING THE PYTHON APPLICATION${RESET}]" && echo
./src/main.py "$@"

exit 0
