import logging
import subprocess
import sys
import os


def run_process_and_print_output(command_as_list: list, stop_on_errors=True):
    try:
        subprocess.run(command_as_list, stderr=sys.stderr, stdout=sys.stdout, text=True, check=stop_on_errors)
    except KeyboardInterrupt:
        logging.error('Keyboard interrupted')
        sys.exit(1)


def path_join(*paths, check=True):
    """
    Does the os.path.join plus checks if the folder really exists or not
    """
    joined_path = os.path.join(*paths)

    if check and not os.path.exists(joined_path):
        raise FileNotFoundError(f'The joined path "{joined_path}" does not exist.')

    return joined_path
