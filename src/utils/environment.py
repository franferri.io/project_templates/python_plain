import os
import sys
import tempfile
from pathlib import Path
import json

from config.config import environment, application
from src.utils.singleton import Singleton
import logging
import tempfile


class Environment(metaclass=Singleton):

    def __init__(self):
        environment['args'] = sys.argv
        environment['application_working_folder'] = tempfile.mkdtemp()
        environment['application_folder'] = os.getcwd()
        environment['current_folder'] = os.getenv('INITIAL_WORKING_DIRECTORY_PATH')
        environment['user_home'] = str(Path.home())
        environment['environment_name'] = os.getenv('ENVIRONMENT_NAME')
        environment['temp_dir'] = tempfile.gettempdir()
        self.setup_logging()

    def setup_logging(self, level=logging.INFO):
        logging_file = os.path.join(environment['temp_dir'], 'toolbox.log')
        print('Logging file being appeded at ' + logging_file)

        logging.basicConfig(
            level=level,  # Set the logging level (DEBUG, INFO, WARNING, ERROR, CRITICAL)
            format='%(asctime)s - %(levelname)s - %(message)s',
            handlers=[
                logging.StreamHandler(),  # Output logs to the console
                logging.FileHandler(logging_file, mode='a')
            ]
        )

        current_level = logging.getLevelName(logging.getLogger().getEffectiveLevel())
        print('Current log level ' + current_level)

    def print_execution_environment_config(self) -> None:
        logging.debug('Execution environment configuration:')
        logging.debug(json.dumps(environment, indent=4))

    def print_application_config(self) -> None:
        logging.debug('Application configuration:')
        logging.debug(json.dumps(application, indent=4))

    def change_dir_to_current_folder(self):
        os.chdir(environment['current_folder'])

    def change_dir_to_application_folder(self):
        os.chdir(environment['application_folder'])

    def check_python_minimum_required_version(self, major: int, minor: int) -> None:
        if sys.version_info < (major, minor):
            logging.error(f"Error: Required Python version {major}.{minor} or higher.")
            message = ''' brew list | grep -i 'python', will show your installed versions
 brew install python@3.12, will install 3.12 if you needit
 brew list python@3.12, will show you where is installed, so you can initialize this project venv using that version
 '''
            logging.error(message)
            sys.exit(1)
