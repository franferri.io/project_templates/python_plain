#!venv/bin/python
from config.config import environment
from src.utils.environment import Environment


def main():
    print('Application ran\n')


if __name__ == "__main__":

    Environment().check_python_minimum_required_version(3, 12)

    Environment().print_execution_environment_config()
    Environment().print_application_config()

    # Run the program relative to the folder where we ran it
    Environment().change_dir_to_application_folder()

    main()
